package yfmp.domain.service;

import java.util.List;

import yfmp.domain.User;
import yfmp.util.exception.InvalidCredentialsException;

public interface UserService {
	public User login(String login, String password) throws InvalidCredentialsException;
	public User register(String name, String password);
	public List<User> getAllUsers();
}
