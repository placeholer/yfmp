package yfmp.domain.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import yfmp.data.UserRepository;
import yfmp.domain.Password;
import yfmp.domain.User;
import yfmp.util.exception.InvalidCredentialsException;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	@NonNull private UserRepository users;
	@NonNull private PasswordRepository passwords;

	@Override
	public User register(String name, String password) {
		User user = new User(name);
		Password pass = new Password(user, password);
		users.save(user);
		passwords.save(pass);
		return user;
	}

	@Override
	public List<User> getAllUsers() {
		return users.findAll();
	}

	@Override
	public User login(String login, String password) throws InvalidCredentialsException {
		User user = users.findByUsername(login).orElseThrow(InvalidCredentialsException::new);
		if(passwords.findByUser(user).orElseThrow(InvalidCredentialsException::new).auth(password)) {
			return user;
		}
		return null;
	}
}
