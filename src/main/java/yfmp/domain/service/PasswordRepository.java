package yfmp.domain.service;

import java.util.Optional;

import org.springframework.data.repository.Repository;

import yfmp.domain.Password;
import yfmp.domain.User;

public interface PasswordRepository extends Repository<Password, Long> {
	public void save(Password p);
	public Optional<Password> findByUser(User user);
	public void deleteByUser(User user);
}
