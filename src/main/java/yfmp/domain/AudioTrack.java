package yfmp.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Data
@NoArgsConstructor
public class AudioTrack {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NonNull
	private String title;

	@ManyToOne
	private Tag artist;

	@ManyToOne
	private Tag album;

	@ManyToOne
	private Tag release;

	private String path;

	public AudioTrack(String title, Tag artist, Tag album, Tag release) {
		this.title = title;
		this.artist = artist;
		this.album = album;
		this.release = release;

		this.path = generatePath();
	}

	public AudioTrack(String title) {
		this(title, null, null, null);
	}

	private String generatePath() {
		// return String.format("%s/%s/%s", artist.getId(), album.getId(), id);
		return "";
	}
}
