package yfmp.domain;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Data
@NoArgsConstructor
@Setter(AccessLevel.PROTECTED)
public class Password {

	@Id
	@GeneratedValue
	private Long id;

	private byte[] salt;

	private byte[] hash;

	@OneToOne
	private User user;

	private static SecureRandom random = new SecureRandom();

	private static final int ITERATIONS = 65536;

	private static final int LENGTH = 128;

	private static final String ALGORITHM = "PBKDF2WithHmacSHA1";

	private static final int SALT_LENGTH = 8;

	public Password(User user, String pass) {
		this.user = user;
		this.salt = generateSalt(SALT_LENGTH);
		this.hash = hashPassword(pass, salt);
	}

	public boolean auth(String pass) {
		return Arrays.equals(hash, hashPassword(pass, salt));
	}


	public static byte[] hashPassword(String pass, byte[] salt) {
		KeySpec spec = new PBEKeySpec(pass.toCharArray(), salt, ITERATIONS, LENGTH);

		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance(ALGORITHM);
			return factory.generateSecret(spec).getEncoded();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] generateSalt(int length) {
		byte[] s = new byte[length];
		random.nextBytes(s);
		return s;
	}
}
