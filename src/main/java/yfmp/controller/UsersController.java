package yfmp.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import yfmp.domain.User;
import yfmp.domain.service.UserService;
import yfmp.util.exception.InvalidCredentialsException;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UsersController {

	@NonNull private UserService service;

	@RequestMapping("/register")
	public User register(@RequestParam String name, @RequestParam String password) {
		return service.register(name, password);
	}

	@RequestMapping("/list")
	public List<User> list() {
		return service.getAllUsers();
	}

	@RequestMapping("/login")
	public User login(@RequestParam String login, @RequestParam String password) {
		try {
			return service.login(login, password);
		} catch(InvalidCredentialsException e) {
			return null;
		}
	}
}
