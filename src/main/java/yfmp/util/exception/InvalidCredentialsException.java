package yfmp.util.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidCredentialsException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidCredentialsException(String msg) {
		super(msg);
	}

	public InvalidCredentialsException(Exception e) {
		super(e);
	}
}
