package yfmp.data;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import yfmp.domain.AudioTrack;
import yfmp.domain.Tag;

public interface AudioTrackRepository extends JpaRepository<AudioTrack, Long> {

	public Optional<AudioTrack> findByTitle(String title);
	public List<AudioTrack> findByArtist(Tag artist);
	public List<AudioTrack> findByAlbum(Tag album);

}
