package yfmp.data;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import yfmp.domain.Tag;

public interface TagRepository extends JpaRepository<Tag, Long> {
	public Optional<Tag> findByTag(String tag);
}
