package yfmp.data;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import yfmp.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {
	public Optional<User> findByUsername(String username);
}
