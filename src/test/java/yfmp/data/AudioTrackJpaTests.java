package yfmp.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import yfmp.domain.AudioTrack;


@DataJpaTest
public class AudioTrackJpaTests {

	@Autowired
	private AudioTrackRepository repo;

	@Test
	public void testCreate() throws Exception {
		AudioTrack track = new AudioTrack("TEST CREATE: " + LocalTime.now());
		repo.save(track);

		assertNotNull(track.getId());
		assertTrue(repo.existsById(track.getId()));
	}

	@Test
	public void testUpdate() throws Exception {
		AudioTrack t = new AudioTrack("TEST UPDATE: " + LocalTime.now());
		repo.save(t);

		t.setTitle("TEST UPDATE: UPDATED " + LocalTime.now());
		repo.save(t);
		assertEquals(t.getTitle(), repo.findById(t.getId()).get().getTitle());
	}

	@Test
	public void testDelete() throws Exception {
		AudioTrack t = new AudioTrack("TEST DELETE: " + LocalTime.now());
		repo.save(t);

		assertTrue(repo.existsById(t.getId()));

		repo.delete(t);
		assertFalse(repo.existsById(t.getId()));
	}
}
