package yfmp.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import yfmp.domain.User;

@DataJpaTest
public class UserJpaTests {

	@Autowired
	private UserRepository repo;

	@Test
	public void testCreate() throws Exception {
		User u = new User("JPA TEST CREATE: " + LocalTime.now());
		repo.save(u);

		assertNotNull(u.getId());
		assertTrue(repo.existsById(u.getId()));
		assertEquals(u.getUsername(), repo.findById(u.getId()).get().getUsername());
	}

	@Test
	public void testUpdate() throws Exception {
		User u = new User("JPA TEST UPDATE: " + LocalTime.now());
		repo.save(u);

		u.setUsername("JPA TEST UPDATE: " + LocalTime.now());

		repo.save(u);

		assertEquals(u.getUsername(), repo.findById(u.getId()).get().getUsername());
	}

	@Test
	public void testDelete() throws Exception {
		User u = new User("JPA TEST UPDATE: " + LocalTime.now());
		repo.save(u);

		assertTrue(repo.existsById(u.getId()));

		repo.delete(u);

		assertFalse(repo.existsById(u.getId()));
	}

	@Test
	public void testFindByUsername() throws Exception {
		User u = new User("TEST FIND BY USERNAME: " + LocalTime.now());
		repo.save(u);

		assertEquals(u.getId(), repo.findByUsername(u.getUsername()).get().getId());
	}
}
