package yfmp.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import yfmp.domain.Tag;

@DataJpaTest
public class TagJpaTests {

	@Autowired
	private TagRepository repo;

	@Test
	public void testCreate() throws Exception {
		Tag t = new Tag("TEST CREATE: " + LocalTime.now());
		repo.save(t);

		assertNotNull(t.getId());
		assertTrue(repo.existsById(t.getId()));
		assertEquals(t.getTag(), repo.findById(t.getId()).get().getTag());
	}

	@Test
	public void testUpdate() throws Exception {
		Tag t = new Tag("TEST UPDATE: " + LocalTime.now());
		repo.save(t);

		t.setTag("TEST UPDATE: UPDATED " + LocalTime.now());
		repo.save(t);
		assertEquals(t.getTag(), repo.findById(t.getId()).get().getTag());
	}

	@Test
	public void testDelete() throws Exception {
		Tag t = new Tag("TEST DELETE: " + LocalTime.now());
		repo.save(t);

		assertTrue(repo.existsById(t.getId()));

		repo.delete(t);
		assertFalse(repo.existsById(t.getId()));
	}
}
